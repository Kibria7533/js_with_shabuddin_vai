function volume(w, h, l) {
    return w * h * l;
  }
  
  volume(4, 6, 3); // 72
  
  //Currying
  function volume(w) {
    return function(h) {
      return function(l) {
        return w * h* l;
      }
    }
  }
  
  volume(4)(6)(3);