var info='{jobTitle:"Software Engineering",age:36, motivation:"no"}';
class Valid extends Error{
    constructor(message){
        super(message);
        this.name=Valid;
    }
}



function readData(data){
    let parsedData=JSON.parse(data)
    if(!parsedData.name){
        throw new Valid('Dont have your name');
    }
}

try{
  let user=  readData('{"jobTitle":"Software Engineering","name":"kibria","age":"36", "motivation":"no"}')
}catch(err){
    console.log(err.message)
}